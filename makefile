CC=gcc #cc es por defecto cc que es compilador de c, pero ahora necesitamos el gnu cc
CFLAGS=-I. #Dice que busque en el mismo directorio los include
DEPS= point.h #Se agrega el .h del cual el .c depende
OBJ= main.o point.o
LIBS=-lm #Hace link con la librer�a matem�tica de gcc, en este caso

%.o: %.c $(DEPS) #se define la regla que el archivo .o depende del .c y del .h definido en DEPS, y lo que necesita es compilar con gcc  con el  nombre de la regla en este caso el archivo .o -->$@, generando los .o --> -c, el $< se refiere a que eso lo haga con la primera dependencia del archivo %.c, el -I. -->CFLAGS
	$(CC) -c -o $@ $< $(CFLAGS)

ejecutable: $(OBJ) #regla para crear el ejecutable, con dependencia de los .o, el $^ escoge todas las dependencias separada por espacio, con el -lm 
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean: #el tarjet clean usado para limpiar los .o y ejecutables, RM tiene predefinido rm
	$(RM) *.o ejecutable  


