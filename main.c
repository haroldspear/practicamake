#include <stdio.h>

#include <point.h>

int main(){
	point p1,p2;
	
	printf("Ingrese x1 :");
	scanf("%f",&p1.x);
	printf("Ingrese y1 :");
	scanf("%f",&p1.y);
	printf("Ingrese z1 :");
	scanf("%f",&p1.z);

	printf("Ahora, Ingrese x2 :");
	scanf("%f",&p2.x);
	printf("Ingrese y2 :");
	scanf("%f",&p2.y);
	printf("Ingrese z2 :");
	scanf("%f",&p2.z);

	printf("Los puntos ingresados son : ");	
	printf("P1={%.1f ,%.1f ,%.1f} , ",p1.x,p1.y,p1.z);
	printf("P2={%.1f ,%.1f ,%.1f} \n",p2.x,p2.y,p2.z);
	
	printf("La distancia entre estos puntos es %.1f \n",calcularDistancia(p1,p2));
}
