#include <math.h>
#include <point.h>

float calcularDistancia(point p1, point p2){
	return sqrt(pow(p1.x-p2.x,2)+pow(p1.y-p2.y,2)+pow(p1.z-p2.z,2));
}
